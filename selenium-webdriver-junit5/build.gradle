plugins {
    id "java"
    id "io.qameta.allure" version "2.9.6"
    id "org.springframework.boot" version "2.6.4"
}

jar {
    archiveBaseName = "selenium-webdriver-junit5"
    archiveVersion = "1.0.0-SNAPSHOT"
}

compileTestJava {
    sourceCompatibility = 1.8
    targetCompatibility = 1.8
    options.compilerArgs += "-parameters"
}

test {
    useJUnitPlatform() {
        if (project.hasProperty("groups")) {
            includeTags "$groups"
        }
        if (project.hasProperty("excludedGroups")) {
            excludeTags "$excludedGroups"
        }
    }

    testLogging {
        events "passed", "skipped", "failed"
        showStandardStreams = true
    }

    systemProperties System.properties

    if (project.hasProperty("excludeTests")) {
        "$excludeTests".split(",").each { excludeTests ->
            exclude excludeTests
        }
    }

    if (project.hasProperty("parallel")) {
        maxParallelForks = Runtime.runtime.availableProcessors()
    }

    ext.failedTests = []

    tasks.withType(Test) {
        afterTest { TestDescriptor descriptor, TestResult result ->
            if(result.resultType == org.gradle.api.tasks.testing.TestResult.ResultType.FAILURE) {
                failedTests << ["${descriptor.className}::${descriptor.name}"]
            }
        }
    }

    gradle.buildFinished {
        if(!failedTests.empty){
            println "Failed test(s) for ${project.name}:"
            failedTests.each { failedTest ->
                println failedTest
            }
        }
    }
}

allure {
    version = "2.16.1"
}


repositories {
    mavenCentral()
    maven {
       url "https://plugins.gradle.org/m2/"
    }
}

ext {
    slf4jVersion  = "1.7.36"
    logbackVersion  = "1.2.11"

    seleniumVersion  = "4.1.2"
    junit5Version = "5.8.2"
    assertjVersion  = "3.22.0"
    wdmVersion = "5.1.0"

    htmlunitVersion = "3.59.0"
    rerunnerVersion = "2.1.6"
    junitPlatformVersion = "1.8.2"
    awaitilityVersion = "4.2.0"
    browsermobVersion = "2.1.5"
    zapclientVersion = "1.10.0"
    axelVersion = "4.4.0"
    selenideVersion = "6.3.4"
    javafakerVersion = "1.0.2"
    extentreportsVersion = "5.0.9"
    cucumberVersion = "7.2.3"
    springBootVersion = "2.6.4"
    appiumVersion = "8.0.0"
    restAssuredVersion = "4.5.1"
}

dependencies {
    implementation("org.slf4j:slf4j-api:${slf4jVersion}")
    implementation("ch.qos.logback:logback-classic:${logbackVersion}")

    testImplementation("org.seleniumhq.selenium:selenium-java:${seleniumVersion}")
    testImplementation("org.junit.jupiter:junit-jupiter:${junit5Version}")
    testImplementation("org.assertj:assertj-core:${assertjVersion}")
    testImplementation("io.github.bonigarcia:webdrivermanager:${wdmVersion}")

    testImplementation("org.seleniumhq.selenium:htmlunit-driver:${htmlunitVersion}") {
        exclude group: "org.seleniumhq.selenium", module: "*"
    }
    testImplementation("org.seleniumhq.selenium:selenium-grid:${seleniumVersion}")
    testImplementation("io.github.artsok:rerunner-jupiter:${rerunnerVersion}")
    testImplementation("org.awaitility:awaitility:${awaitilityVersion}")
    testImplementation("org.junit.platform:junit-platform-launcher:${junitPlatformVersion}")
    testImplementation("net.lightbody.bmp:browsermob-core:${browsermobVersion}") {
        exclude group: "com.fasterxml.jackson.core", module: "*"
    }
    testImplementation("org.zaproxy:zap-clientapi:${zapclientVersion}")
    testImplementation("com.deque.html.axe-core:selenium:${axelVersion}") {
        exclude group: "org.seleniumhq.selenium", module: "*"
    }
    testImplementation("com.codeborne:selenide:${selenideVersion}") {
        exclude group: "org.seleniumhq.selenium", module: "selenium-java"
        exclude group: "io.github.bonigarcia", module: "webdrivermanager"
        exclude group: "org.apache.httpcomponents.client5", module: "httpclient5"
    }
    testImplementation("com.github.javafaker:javafaker:${javafakerVersion}")
    testImplementation("com.aventstack:extentreports:${extentreportsVersion}") {
        exclude group: "org.projectlombok", module: "lombok"
    }
    testImplementation("org.junit.platform:junit-platform-suite:${junitPlatformVersion}")
    testImplementation("io.cucumber:cucumber-java:${cucumberVersion}")
    testImplementation("io.cucumber:cucumber-junit-platform-engine:${cucumberVersion}")

    implementation("org.springframework.boot:spring-boot-starter:${springBootVersion}")
    implementation("org.springframework.boot:spring-boot-starter-web:${springBootVersion}")
    testImplementation("org.springframework.boot:spring-boot-starter-test:${springBootVersion}")
    testImplementation("io.appium:java-client:${appiumVersion}") {
        exclude group: "org.seleniumhq.selenium", module: "*"
    }
    testImplementation("io.rest-assured:rest-assured:${restAssuredVersion}")
}
